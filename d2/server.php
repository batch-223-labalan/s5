<?php
session_start();
// echo $_POST['description'];
// echo $_SESSION['greet'];

class TaskList{
    // add Task
    public function add($description){
        $newTask = (object)[
            'description' => $description,
            'isFinished' => false
        ];
        //if there are no task
        if($_SESSION['tasks']=== null){
            $_SESSION['tasks'] = array();
        }
        array_push($_SESSION['tasks'],$newTask);
    }

    //Update Task
    // the update task will be needing 3 parameters:
        // $id

    public function update($id, $description, $isFinished){
        $_SESSION['tasks'][$id]->description = $description;
        $_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;
    }

    public function remove($id){
        // array_splice(array, startDel, length, newArrElement)
        array_splice($_SESSION['tasks'], $id, 1);
    }

    public function clear(){
        session_destroy();
    }
}
// taskList is instantiated from the TaskList() class to have access with its method
$taskList = new TaskList();

//This will handle the action sent by the user.
if($_POST['action'] === "add"){
    $taskList->add($_POST['description']);
}
else if ($_POST['action']=== 'update'){
    $taskList->update($_POST['id'],$_POST['description'],$_POST['isFinished']);
}
else if ($_POST['action']==='remove'){
    $taskList->remove($_POST['id']);
}
else if ($_POST['action']==='clear'){
    $taskList->clear();
}

//it will redirect us to the index file upon sending the request
header('Location: ./index.php');

